package mytask.debug;

/**
 * Find min element.
 */
public class MinElementSimple {
    public static void main(String[] args) {
        int[] arr = {2,5,7,3,2,4,6,-5,7,2,0};
        int min;
        if (arr.length > 0){
            min = arr[0];
            if (arr.length == 1){
                System.out.println(min);
                return;
            } for (int i = 1; i < arr.length; i++) {
                if (arr[i] < min){
                    min = arr[i];
                }
            }
            System.out.println(min);
        }
    }
}
