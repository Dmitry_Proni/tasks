package leetcode.algorithms.a344reversestring;

/**
 * Created by nayil on 27.10.17.
 */
public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.reverseString("hello"));
    }
}
