package leetcode.algorithms.a557reversewordsinstring3;

/**
 * Given a string, you need to reverse the order of characters in each word within a sentence while still preserving whitespace
 * and initial word order.
 * Example 1:
 * Input: "Let's take LeetCode contest"
 * Output: "s'teL ekat edoCteeL tsetnoc"
 */
public class Main {
    public static void main(String[] args) {
        String str = "abc def";
        SolutionInterface solution1 = new Solution();
        System.out.println(solution1.reverseWords(str));
    }
}
