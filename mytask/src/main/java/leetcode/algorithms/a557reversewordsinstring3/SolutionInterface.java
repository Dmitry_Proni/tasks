package leetcode.algorithms.a557reversewordsinstring3;

/**
 * Created by nayil on 06.09.17.
 */
public interface SolutionInterface {
    String reverseWords(String s);
}
