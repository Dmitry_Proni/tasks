package leetcode.algorithms;

/**
 * Created by nayil on 03.09.17.
 */
public interface A500KeyboardRowInterface {
    String[] findWords(String[] words);
}
