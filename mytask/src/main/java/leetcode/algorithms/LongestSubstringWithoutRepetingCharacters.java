package leetcode.algorithms;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nayil on 23.08.17.
 */
public class LongestSubstringWithoutRepetingCharacters {
    public static void main(String[] args) {
        String s1 = "abcbabb";
        System.out.println(lengthOfLongestSubstring(s1));
        System.out.println("------------------------------");
        s1 ="bbbb";
        System.out.println(lengthOfLongestSubstring(s1));
        System.out.println("------------------------------");
        s1 ="pwwkew";
        System.out.println(lengthOfLongestSubstring(s1));

    }
    private static String lengthOfLongestSubstring(String s){
        String result = "";
        StringBuilder strBuilder = new StringBuilder();
        Map<Character, Integer> wordMap = new HashMap<>();
        Map<String, Integer> resultMap = new HashMap<>();
        char[] chars = s.toCharArray();
        for (int i = 0; i < s.length(); i++) {
            wordMap = new HashMap<>();
            for (int j = i; j < s.length(); j++) {
                if(wordMap.get(chars[j]) == null){
                    strBuilder.append(chars[j]);
                    wordMap.put(chars[j], j);
                }else{
                    break;
                }
            }
            resultMap.put(strBuilder.toString(), strBuilder.capacity());
            strBuilder.delete(0, strBuilder.capacity());
        }
        for(Map.Entry<String, Integer> entry : resultMap.entrySet()){
            if (result.length() < entry.getKey().length()) {
                result = entry.getKey();
            }
            System.out.println(entry.getKey());
        }
        System.out.println("------------------------------");
        return result;
    }
}
