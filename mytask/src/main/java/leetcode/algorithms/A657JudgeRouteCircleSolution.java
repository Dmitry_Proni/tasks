package leetcode.algorithms;

import java.util.Stack;

/**
 * Created by nayil on 24.08.17.
 */
public class A657JudgeRouteCircleSolution {
    //Dima
    public boolean judgeCircleDima(String moves) {
        int v = 0, h = 0;
        for (char c : moves.toCharArray()) {
            if(c == 'U') v++;
            else if(c == 'D') v--;
            else if(c == 'R') h++;
            else if(c == 'L') h--;
        }
        return v == h && h == 0;
    }

    //does not cover all variants. Doesn't work with URDL
    public boolean judgeCircle(String moves) {
        char[] charsMoves = moves.toCharArray();
        Stack<Character> stack = new Stack();
        for (int i = 0; i < charsMoves.length; i++) {
            if(!stack.empty()){
                if(stack.peek() == charsMoves[i]){
                    stack.pop();
                }else {
                    stack.push(charsMoves[i]);
                }
            }else {
                stack.push(charsMoves[i]);
            }
        }
        return stack.empty();
    }
}
